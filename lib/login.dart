import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
// import 'menulist.dart';

import 'dart:developer';

import 'main.dart';

// final FirebaseDatabase database = FirebaseDatabase();
final database = Firestore.instance;

class Login extends StatefulWidget {
  @override
  _Login createState() => _Login();
}

class _Login extends State<Login> {
  User user;
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  
  handleLogin(ctxt) async {
    user = User("","");

    final FormState form = formkey.currentState;
    form.save();

    QuerySnapshot result  = await database.collection("users")
      .where('username', isEqualTo: user.username)
      .where('password', isEqualTo: user.password).getDocuments();

    final List<DocumentSnapshot> documents = result.documents;

    if( documents.length == 1 ){
      Navigator.push(ctxt, new MaterialPageRoute(
        builder: (c) => new MyHomePage(title: 'Cook Book Starter'))
      );
    }
    else{
      showDialog(
        context: context,
        builder: (BuildContext context){
            return AlertDialog(
              title: Text("Error", textAlign: TextAlign.center),
              content: Text("User authentication failed.", textAlign: TextAlign.center)
            );
        }
      );
    }
  }

  @override
  Widget build (BuildContext ctxt) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Cook Book Starter"),
      ),
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(image: new AssetImage("assets/images3.jpg"), fit: BoxFit.cover,),
        ),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 100.0),
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Flexible(
                    flex: 0,
                    child: Center(
                      child: Form(
                        key: formkey,
                        child: Flex(
                          direction: Axis.vertical,
                          children: <Widget>[
                            ListTile(
                              title: TextFormField(
                                initialValue: "",
                                onSaved: (val) => user.username = val,
                                decoration: const InputDecoration(
                                  icon: Icon(Icons.email),
                                  labelText: 'Username/Email',
                                )
                              )
                            ),
                            ListTile(
                              title: TextFormField(
                                initialValue: "",
                                onSaved: (val) => user.password = val,
                                obscureText: true,
                                decoration: const InputDecoration(
                                  icon: Icon(Icons.lock),
                                  labelText: 'Password'
                                )
                              )
                            ),
                            Row(
                              children: <Widget>[
                                FlatButton(
                                  onPressed: (){
                                    handleLogin(ctxt);
                                  },
                                  child: Text(
                                    "Login",
                                  ),
                                ),
                                FlatButton(
                                  onPressed: (){
                                    Navigator.push(
                                      ctxt,
                                      new MaterialPageRoute(builder: (ctxt) => new Registration()),
                                    );
                                  },
                                  child: Text(
                                    "Register",
                                  ),
                                )
                              ]
                            )
                          ],
                        )
                      )
                    )
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Registration extends StatefulWidget {
  final String title;

  Registration({Key key, this.title}) : super(key: key);

  @override
  _Registration createState() => _Registration();
}

class _Registration extends State<Registration> {
  DatabaseReference itemRef;
  User user;
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();

  @override
  void initState(){
    super.initState();

    // itemRef = database.reference().child('users');
    // itemRef.onChildAdded.listen(_onEntryAdded);
    // itemRef.onChildChanged.listen(_onEntryChanged);
  }

  void handleSubmit() async {
    user = User("","");

    // await database.collection("users")
    //   .document()
    //   .setData({
    //     'title': 'Mastering Flutter',
    //     'description': 'Programming Guide for Dart'
    //   });

    // DocumentReference ref = await database.collection("users")
    // database.collection("users").add({
    //   'title': 'Flutter in Action',
    //   'description': 'Complete Programming Guide to learn Flutter'
    // });


    final FormState form = formkey.currentState;
    
    form.save();
    form.reset();
    database.collection("users").add(user.toJson());

    showDialog(
        context: context,
        builder: (BuildContext context){
            return AlertDialog(
              title: Text("Success", textAlign: TextAlign.center),
              content: Text("User successfully registered.", textAlign: TextAlign.center)
            );
        }
      );
  }
  
  @override
  Widget build (BuildContext ctxt) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Registration"),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            flex: 0,
            child: Center(
              child: Form(
                key: formkey,
                child: Flex(
                  direction: Axis.vertical,
                  children: <Widget>[
                    ListTile(
                      title: TextFormField(
                        initialValue: "",
                        onSaved: (val) => user.username = val,
                        decoration: const InputDecoration(
                          icon: Icon(Icons.email),
                          labelText: 'Username/Email'
                        )
                      )
                    ),
                    ListTile(
                      title: TextFormField(
                        initialValue: "",
                        onSaved: (val) => user.password = val,
                        decoration: const InputDecoration(
                          icon: Icon(Icons.lock),
                          labelText: 'Password'
                        )
                      )
                    ),
                    FlatButton(
                      onPressed: (){
                        handleSubmit();
                      },
                      child: Text(
                        "Register Now",
                      ),
                    )
                  ]
                )
              )
            )
          )
        ]
      )
    );
  }
}

class User {
  String key;
  String username;
  String password;
  
  User(this.password, this.username);

  User.fromSnapshot(DataSnapshot snapshot)
    : key = snapshot.key,
      username = snapshot.value["username"],
      password = snapshot.value["password"];

  toJson(){
    return {
      "username": username,
      "password": password
    };
  }
}