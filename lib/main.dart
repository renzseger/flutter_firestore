import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'dart:developer';

import 'login.dart';
import 'menusingle.dart';

void main() => runApp(MyApp());

final database = Firestore.instance;

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: Login(),
      // home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

Widget _buildList(BuildContext context, DocumentSnapshot document) {
    return ListTile(
      title: Text(document['title']),
      trailing: IconButton(
        icon: Icon(Icons.delete),
        onPressed: (){
          database.collection("menus").document(document.documentID).delete();
        },
      ),
      onTap: (){
        Navigator.push(context, new MaterialPageRoute(
          builder: (c) => new MenuSingle(title: document['title'], documentId: document.documentID))
        );
      },
      // subtitle: Text(document['password']),
    );
  }

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Item> items = List();
  Item item;
  // DatabaseReference itemRef;
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();

  @override
  void initState(){
    super.initState();
    item = Item("", "");
  }

  void handleSubmit() async {
    final FormState form = formkey.currentState;
    
    if(form.validate()){
      form.save();
      form.reset();

      database.collection("menus").add(item.toJson());

      showDialog(
        context: context,
        builder: (BuildContext context){
            return AlertDialog(
              title: Text("Success", textAlign: TextAlign.center),
              content: Text("Your Work Book successfully saved.", textAlign: TextAlign.center)
            );
        }
      );
    }
  }

  void handleRemove(String documentId){
    // log(documentId);
    // database.reference().child('items').child(documentId).remove();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            flex: 0,
            child: Center(
              child: Form(
                key: formkey,
                child: Flex(
                  direction: Axis.vertical,
                  children: <Widget>[
                    ListTile(
                      title: TextFormField(
                        initialValue: "",
                        onSaved: (val) => item.title = val,
                        validator: (val) => val == "" ? val : null,
                        decoration: InputDecoration(
                          hintText: "Cook Book Title",
                        )
                      )
                    ),
                    ListTile(
                      title: Container(
                        // hack textfield height
                        child: TextFormField(
                          maxLines: 10,
                          initialValue: "",
                          onSaved: (val) => item.description = val,
                          validator: (val) => val == "" ? val : null,
                          decoration: InputDecoration(
                              hintText: "Cook Book Description",
                              border: OutlineInputBorder(),
                          ),
                        ),
                      )
                    ),
                    FlatButton(
                      child: Text(
                        "Submit",
                      ),
                      onPressed: (){
                        handleSubmit();
                      }
                    )
                  ],
                ),
              ),
            ),
          ),
          Flexible(
            child: StreamBuilder(
                stream: Firestore.instance.collection('menus').snapshots(),
                //print an integer every 2secs, 10 times
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Text("Loading..");
                  }
                  return ListView.builder(
                    itemExtent: 80.0,
                    itemCount: snapshot.data.documents.length,
                    itemBuilder: (context, index) {
                      return _buildList(context, snapshot.data.documents[index]);
                    },
                  );
                },
              )
          )
        ],
      ),
    );
  }
}

class Item {
  String key;
  String title;
  String description;
  
  Item(this.title, this.description);

  Item.fromSnapshot(DataSnapshot snapshot)
    : key = snapshot.key,
      title = snapshot.value["title"],
      description = snapshot.value["description"];

  toJson(){
    return {
      "title": title,
      "description": description
    };
  }
}