import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'dart:developer';

import 'login.dart';

void main() => runApp(MyApp());

final FirebaseDatabase database = FirebaseDatabase();

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cooking Receipt and Menu',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: Login(),
      // home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Item> items = List();
  Item item;
  DatabaseReference itemRef;
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();

  @override
  void initState(){
    super.initState();
    item = Item("","", "");
    // final FirebaseDatabase database = FirebaseDatabase(app: app);
    itemRef = database.reference().child('items');
    itemRef.onChildAdded.listen(_onEntryAdded);
    itemRef.onChildChanged.listen(_onEntryChanged);
    // itemRef.once().then((DataSnapshot snapshot){
    //   Map<dynamic, dynamic> values = snapshot.value;
    //     values.forEach((key,values) {
    //       print(key +": "+ values["title"] +": "+ values["body"]);
    //     });
    // });
  }

  _onEntryAdded(Event event){
    setState(() {
      items.add(Item.fromSnapshot(event.snapshot));
    });
  }

  _onEntryChanged(Event event){
    var old = items.singleWhere((entry){
      return entry.key == event.snapshot.key;
    });
    setState(() {
      items[items.indexOf(old)] = Item.fromSnapshot(event.snapshot);
    });
  }

  void handleSubmit(){
    final FormState form = formkey.currentState;
    
    if(form.validate()){
      form.save();
      form.reset();
      itemRef.push().set(item.toJson());
    }
  }

  void handleRemove(String documentId){
    // log(documentId);
    database.reference().child('items').child(documentId).remove();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            flex: 0,
            child: Center(
              child: Form(
                key: formkey,
                child: Flex(
                  direction: Axis.vertical,
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.info),
                      title: TextFormField(
                        initialValue: "",
                        onSaved: (val) => item.title = val,
                        validator: (val) => val == "" ? val : null,
                      )
                    ),
                    ListTile(
                      leading: Icon(Icons.info),
                      title: TextFormField(
                        initialValue: "",
                        maxLines: null,
                        keyboardType: TextInputType.multiline,
                        onSaved: (val) => item.body = val,
                        validator: (val) => val == "" ? val : null,
                      )
                    ),
                    IconButton(
                      icon: Icon(Icons.send),
                      onPressed: (){
                        handleSubmit();
                      },
                    )
                  ],
                ),
              ),
            ),
          ),
          Flexible(
            child: FirebaseAnimatedList(
              query: itemRef,
              itemBuilder: (
                BuildContext context,
                DataSnapshot snapshot,
                Animation<double> animation,
                int index
              ){
                return new ListTile(
                  leading: Icon(Icons.message),
                  title: Text(items[index].title),
                  // subtitle: Text(items[index].body),
                  trailing: FlatButton(
                    onPressed: () {
                      handleRemove(items[index].key);
                    },
                    child: Text(
                      "Remove",
                    ),
                  )
                );
              }
            ),
          )
        ],
      ),
    );
  }
}

class Item {
  String key;
  String title;
  String body;
  
  Item(this.key, this.title, this.body);

  Item.fromSnapshot(DataSnapshot snapshot)
    : key = snapshot.key,
      title = snapshot.value["title"],
      body = snapshot.value["body"];

  toJson(){
    return {
      "key": key,
      "title": title,
      "body": body
    };
  }
}